import React, { Component } from 'react'
import { Container , Form, Row, Col, Button} from 'react-bootstrap'

export default class Update extends Component {
    render() {
        return (
            <Container style={{ marginTop: '100px' }}>
                <h1>Update AOI</h1>
                <Form style={{ margin: '50px' }}>
                    <Form.Row>
                        <Col>
                        <Form.Control placeholder="AOI Area" value="null"/>
                        </Col>
                        <Col>
                        <Form.Control placeholder="AOI Name" value="null"/>
                        </Col>
                        <Col>
                        <Form.Control placeholder="AOI Date" value="null"/>
                        </Col>
                        <Col>
                        <Form.Control placeholder="AOI Properties" value="null"/>
                        </Col>
                    </Form.Row>
                    <Button style={{ margin: '30px', float: 'right' }}>Update AOI</Button>
                </Form>
            </Container>
        )
    }
}
