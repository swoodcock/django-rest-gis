import React from 'react';
import './App.css';
import { Route, Switch ,Link, BrowserRouter as Router } from 'react-router-dom';
import Home from './Home';
import Update from './Update';
import Create from './Create';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'

if ( process.env.NODE_ENV == 'development') {
  window.API_URL = 'http://' + process.env.REACT_APP_HOST_URL + ':50001';
} else {
  window.API_URL = process.env.REACT_APP_HOST_URL;
};

function App() {
    return (
      <Router>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">DRF-GIS</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href='/'>Home</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/aoi/create" component={Create} />
          <Route path="/aoi/${item.id}/update" component={Update} />
      </Switch>
    </Router>
    );
  }

  export default App;
