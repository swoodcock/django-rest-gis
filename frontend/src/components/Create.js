import React, { Component } from 'react'
import { Container , Form, Row, Col, Button} from 'react-bootstrap'

export default class Create extends Component {
    render() {
        return (
            <Container style={{ marginTop: '100px' }}>
                <h1>Add AOI</h1>
                <Form style={{ margin: '50px' }}>
                    <Form.Row>
                        <Col>
                        <Form.Control placeholder="AOI Area" />
                        </Col>
                        <Col>
                        <Form.Control placeholder="AOI Name" />
                        </Col>
                        <Col>
                        <Form.Control placeholder="AOI Date" />
                        </Col>
                        <Col>
                        <Form.Control placeholder="AOI Properties" />
                        </Col>
                    </Form.Row>
                    <Button style={{ margin: '30px', float: 'right' }}>Add AOI</Button>
                </Form>
            </Container>
        )
    }
}
