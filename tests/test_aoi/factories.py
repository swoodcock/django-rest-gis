import json

import factory
from django.contrib.gis.geos import Polygon
from factory import fuzzy

from aoi.models import AOI


class FuzzyPolygon(fuzzy.BaseFuzzyAttribute):
    """Yields random polygon."""

    def __init__(self, length=None, **kwargs):
        """Generate random polygon length."""
        if length is None:
            length = factory.random.randgen.randrange(3, 20, 1)
        if length < 3:
            raise Exception("Polygon needs to be 3 or greater in length.")
        self.length = length
        super().__init__(**kwargs)

    def get_random_coords(self):
        """Generate random coordinate for polygon."""
        return (
            factory.Faker("latitude").generate({}),
            factory.Faker("longitude").generate({}),
        )

    def fuzz(self):
        """Close polygon with same start / end point."""
        prefix = suffix = self.get_random_coords()
        coords = [self.get_random_coords() for __ in range(self.length - 1)]
        return Polygon([prefix] + coords + [suffix])


class JSONFactory(factory.DictFactory):
    """Use with factory.Dict to make JSON strings."""

    @classmethod
    def _generate(cls, create, attrs):
        obj = super()._generate(create, attrs)
        return json.dumps(obj)


class AOIFactory(factory.DjangoModelFactory):
    """A Factory to generate mock AOIFactory objects to be used in tests."""

    class Meta:
        model = AOI

    area = FuzzyPolygon()
    name = factory.Faker("random_lowercase_letter")
    date = factory.Faker("date_time_this_year")
    properties = factory.Dict(
        {
            "first_name": factory.Faker("first_name"),
            "company": factory.Faker("company"),
            "random_num": factory.Faker("random_int"),
        },
        dict_factory=JSONFactory,
    )
