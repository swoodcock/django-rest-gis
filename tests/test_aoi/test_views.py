import json

import factory
import pytest
from django.core.serializers.json import DjangoJSONEncoder
from django.urls import reverse
from django_mock_queries.mocks import MockSet

from aoi.models import AOI
from aoi.views import AOIViewSet
from tests.test_aoi.factories import AOIFactory

pytestmark = [pytest.mark.urls("assessment.urls"), pytest.mark.unit]


class TestAOIViewSet:
    def test_list(self, mocker, rf):
        # Arrange
        url = reverse("aoi-list")
        request = rf.get(url)
        qs = MockSet(AOIFactory.build(), AOIFactory.build(), AOIFactory.build())
        view = AOIViewSet.as_view({"get": "list"})
        # Mock
        mocker.patch.object(AOIViewSet, "get_queryset", return_value=qs)
        # Act
        response = view(request).render()
        # Assert
        assert response.status_code == 200
        assert len(json.loads(response.content)) == 3

    @pytest.mark.django_db
    def test_create(self, mocker, rf):
        valid_data_dict = factory.build(dict, FACTORY_CLASS=AOIFactory)
        url = reverse("aoi-list")
        request = rf.post(
            url,
            content_type="application/json",
            data=json.dumps(valid_data_dict, cls=DjangoJSONEncoder),
        )
        mocker.patch.object(AOI, "save")
        view = AOIViewSet.as_view({"post": "create"})

        response = view(request).render()

        assert response.status_code == 201
        assert json.loads(response.content) == valid_data_dict

    @pytest.mark.django_db
    def test_retrieve(self, mocker, rf):
        aoi = AOIFactory.create()
        expected_json = {
            "id": aoi.id,
            "area": aoi.area.wkt,
            "name": aoi.name,
            "date": aoi.date,
            "properties": aoi.properties,
        }
        url = reverse("aoi-detail", kwargs={"pk": aoi.id})
        request = rf.get(url)
        mocker.patch.object(AOIViewSet, "get_queryset", return_value=MockSet(AOI))
        view = AOIViewSet.as_view({"get": "retrieve"})

        response = view(request, pk=aoi.id).render()

        assert response.status_code == 200
        assert json.loads(response.content) == expected_json

    @pytest.mark.django_db
    def test_update(self, mocker, rf):
        old_aoi = AOIFactory.create()
        new_aoi = AOIFactory.build()
        aoi_dict = {
            "area": new_aoi.area.wkt,
            "name": new_aoi.name,
            "date": new_aoi.date,
            "properties": new_aoi.properties,
        }
        url = reverse("aoi-detail", kwargs={"pk": old_aoi.id})
        request = rf.put(
            url,
            content_type="application/json",
            data=json.dumps(aoi_dict, cls=DjangoJSONEncoder),
        )
        mocker.patch.object(AOIViewSet, "get_object", return_value=old_aoi)
        mocker.patch.object(AOI, "save")
        view = AOIViewSet.as_view({"put": "update"})

        response = view(request, pk=old_aoi.id).render()

        assert response.status_code == 200
        assert json.loads(response.content) == aoi_dict

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "field",
        [
            ("area"),
            ("name"),
            ("date"),
            ("properties"),
        ],
    )
    def test_partial_update(self, mocker, rf, field):
        aoi = AOIFactory.create()
        aoi_dict = {
            "area": aoi.area.wkt,
            "name": aoi.name,
            "date": aoi.date,
            "properties": aoi.properties,
        }
        valid_field = aoi_dict[field]
        url = reverse("aoi-detail", kwargs={"pk": aoi.id})
        request = rf.patch(
            url,
            content_type="application/json",
            data=json.dumps({field: valid_field}, cls=DjangoJSONEncoder),
        )
        mocker.patch.object(AOIViewSet, "get_object", return_value=aoi)
        mocker.patch.object(AOI, "save")
        view = AOIViewSet.as_view({"patch": "partial_update"})

        response = view(request).render()

        assert response.status_code == 200
        assert json.loads(response.content)[field] == valid_field

    @pytest.mark.django_db
    def test_delete(self, mocker, rf):
        aoi = AOIFactory.create()
        url = reverse("aoi-detail", kwargs={"pk": aoi.id})
        request = rf.delete(url)
        mocker.patch.object(AOIViewSet, "get_object", return_value=aoi)
        del_mock = mocker.patch.object(AOI, "delete")
        view = AOIViewSet.as_view({"delete": "destroy"})

        response = view(request).render()

        assert response.status_code == 204
        assert del_mock.assert_called
