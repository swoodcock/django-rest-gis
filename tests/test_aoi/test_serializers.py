import factory
import pytest
from django.contrib.gis.geos import GEOSGeometry
from rest_framework import serializers

from aoi.serializers import AOISerializer
from tests.test_aoi.factories import AOIFactory


class TestAOISerializer:
    @pytest.mark.unit
    def test_serialize_model(self):
        project = AOIFactory.build()
        serializer = AOISerializer(project)

        assert serializer.data

    @pytest.mark.unit
    @pytest.mark.django_db
    def test_serialized_data(self, mocker):
        valid_serialized_data = factory.build(dict, FACTORY_CLASS=AOIFactory)

        serializer = AOISerializer(data=valid_serialized_data)

        assert serializer.is_valid()
        assert serializer.errors == {}

    @pytest.mark.unit
    def test_empty_overlap_geoms(self):
        empty_overlap_geoms_record = factory.build(
            dict,
            FACTORY_CLASS=AOIFactory,
            overlap_geoms=GEOSGeometry("POLYGON EMPTY"),
        )

        with pytest.raises(serializers.ValidationError):
            serializer = AOISerializer(data=empty_overlap_geoms_record)
            serializer.validate(data=empty_overlap_geoms_record)
