# **Complete 09/07/2021**
DRF CRUD endpoints for manipulating geospatial / AOI data.
Simple frontend implemented for additional development.

**Run production server:**
- Clone repo
- Edit .env and .runtime.env (& docker-compose.yml if required)
- Run `bash build_scrip.sh`
- Run `docker-compose up -d`
- Access at localhost:50050

**Run development server:**
- Run `./manage.py makemigrations aoi && ./manage.py migrate`.
- If managing via django-admin, also create a superuser account
  - `python manage.py createsuperuser`
- In _frontend_ folder, install node_modules with `npm install`
- In _frontend_ folder, run **webpack** with `npm run dev`.
- In project home, run `./manage.py runserver`.
- Access at localhost:8000 & localhost:8000/api/aoi

**Add AOIs:**
- Create AOIs via script with a variety of geometry encodings (e.g. WKT)
  - http://localhost:8000/api/aoi/
- Use built in django-admin panel to add geometries interactively:
  - http://localhost:8000/admin/
  - For the dev server, credentials are generated by the user.
  - For the prod server, credentials are generated by .runtime.env values.
- View http://127.0.0.1:8000/api/aoi to list the just created aois (http://127.0.0.1:8000 for the React client).

**Search endpoints:**
- Each field is searchable at the /api/aoi endpoint, with exact key:value pairs.
  - http://localhost:8080/api/aoi?<KEY>=<VALUE>&<KEY>=<VALUE>
  - Nested JSON values can also be searched within the 'properties' field.
  - For this to work, three things must be in place:
    - Queryset annotations for each nested property in views.AOIListCreate.
    - A variable for the property, with property type, in filters.PropertiesFilter.
    - The property field included in filters.PropertiesFilter.Meta.
- The 'area' field can be filtered by the geom_intersect URL param, via textual types supported by GEOSGeometry
  - WKT, HEXEWKB, WKB (in a buffer), and GeoJSON.
  - Must include the SRID.
- In addition, a bbox can be used in the search:
  - Includes overlapping polygons
  - /api/aoi/?in_bbox=-180,-90,180,90
- If the overlap geometries are required only, specify overlap_only=true in the URL.
- Fuzzy value searching is enabled from the /api/aoi?search endpoint.
  - http://localhost:8080/api/aoi?search=2020
  - Enabled for 'id', 'date' and 'name' fields.
  - Filtering is also enabled for nested values within the 'properties' field.
- Ordering can be specified manually from the /api/aoi?ordering:
  - http://localhost:8080/api/aoi?ordering=[name,date]
  - Enabled for 'id', 'date' and 'name' fields.

**Notes:**
- The Dockerfile uses a pre-built corporate base image, including CA certs and proxy variables.
- In an environment with no such restrictions, set REG_URL to "docker.io" and PROXY_URL="".


# Assignment details

Build a RESTful API service, the service should handle the following objects and actions:
1. object Area, which has the following properties:
  a. area: valid polygon, list of points, closed (the first point and the last are the same), the lines do not cross themselves.
  b. name: string (unique).
  c. date.
  d. properties: dictionary of arguments.
2. the service should be able to save, retrieve, and delete Area objects.
3. the objects lists should be searchable by:
  a. name - search input: string, any area containing the string should be returned, results should be returned ordered by name.
  b. area - search input: valid polygon, any area which intersects with the polygon should be returned, ordered by size from the larger to the smallest.
  c. same as (b): but the returned areas are only the intersection between the input polygon and the and the object's area.
  d. property - search input dictionary of properties and values, any object that has all the matching properties should be returned, ordered by data, the expected result is complete system which can be run with docker compose, the code must include unit test, and some complete integration tests.

# Limitations

- Security consideration:
  - Change to non-privileged user for database (not postgres).
  - Use secrets instead of hardcoded values .env files.
- Frontend has very little development - improve:
  - Small Leaflet.js map to create AOIs, then add via DRF endpoint.
  - Bootstrap data tables.
- More data validation for URL param inputs:
  - Include check to see if geom_intersect includes an SRID.
  - Check if valid key / values.
- Additional testing required:
  - Existing tests can be ran with $ pytest (incomplete).
  - URL params geom_intersects & overlap_only, using various test geometries.
  - E.g. Large Geom: /api/aoi/?overlap_only=true&geom_intersect=SRID=4326;POLYGON((-172.82226584852%20-74.545894861221,%20-174.22851584852%2078.032230138779,%20177.33398415148%2077.329105138779,%20173.11523415148%20-73.842769861221,%20-172.82226584852%20-74.545894861221))
  - E.g. Small Geom: /api/aoi/?overlap_only=true&geom_intersect=SRID=4326;POLYGON((-173.52539084852%20-52.749019861221,%20-171.41601584852%20-57.670894861221,%20-167.19726584852%20-52.749019861221,%20-171.41601584852%20-48.530269861221,%20-173.52539084852%20-52.749019861221))
