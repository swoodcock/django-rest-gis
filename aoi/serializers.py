from django.contrib.gis.geos import GEOSGeometry
from rest_framework.serializers import ModelSerializer, SerializerMethodField

from aoi.models import AOI


class AOISerializer(ModelSerializer):
    """Serialize AOI model to JSON."""

    area = SerializerMethodField()

    class Meta:
        model = AOI
        fields = "__all__"

    def get_area(self, obj):
        """
        Function to replace AOI area with overlap geometries from
        intersection with user input AOI, if specified in URL.
        """

        if getattr(obj, "overlap_geoms", False) is False:
            return obj.area.wkt
        if (overlap_geoms := GEOSGeometry(obj.overlap_geoms.wkt)) == "POLYGON EMPTY":
            return "null"
        else:
            return overlap_geoms.wkt
