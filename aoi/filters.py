from django_filters import rest_framework as filters
from rest_framework_gis.filters import GeometryFilter
from rest_framework_gis.filterset import GeoFilterSet

from aoi.models import AOI


class AreaFilter(GeoFilterSet):
    """Filter for intersection of area geometry, via URL geom_intersect=value."""

    geom_intersect = GeometryFilter(name="area", lookup_expr="intersects")

    class Meta:
        model = AOI
        fields = ["name", "date"]


class PropertiesFilter(filters.FilterSet):
    """Filter to add search functionality for nested JSON 'properties' field."""

    property1 = filters.CharFilter(lookup_expr="iexact")
    property2 = filters.CharFilter(lookup_expr="iexact")

    class Meta:
        model = AOI
        fields = ["property1", "property2"]
