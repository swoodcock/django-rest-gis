from django.contrib.gis.db.models.functions import Intersection
from django.contrib.gis.geos import GEOSGeometry
from django.db.models import ExpressionWrapper, F, FloatField, Func
from django.db.models.fields.json import KeyTextTransform
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status, viewsets
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.response import Response
from rest_framework_gis.filters import InBBoxFilter

from aoi.filters import PropertiesFilter
from aoi.models import AOI
from aoi.serializers import AOISerializer


class AOIListCreate(generics.ListCreateAPIView):
    """DRF view to CRUD AOI model."""

    # Annotate queryset with properties JSON values for filtering
    queryset = AOI.objects.annotate(
        property1=KeyTextTransform("property1", "properties")
    ).annotate(property2=KeyTextTransform("property2", "properties"))
    serializer_class = AOISerializer
    filter_backends = DjangoFilterBackend, InBBoxFilter, SearchFilter, OrderingFilter
    filterset_class = PropertiesFilter
    search_fields = ["id", "name", "date", "properties"]
    ordering_fields = ["id", "name", "date", "properties"]
    filterset_fields = ("id", "name", "date")
    bbox_filter_field = "area"
    bbox_filter_include_overlapping = True

    def get_queryset(self):
        """
        Annotate queryset with geometry sizes and overlap geometries,
        if URL param option specified in each case.
         must include the SRID encoded in the geometry.

        Parameters
        ----------
        geom_intersect : str
            Bounding geometry to filter AOI datasey by.
            Accepts textual types supported by GEOSGeometry.
            (WKT, HEXEWKB, WKB (in a buffer), and GeoJSON).
        overlap_only : bool
            If true, returns only the overlapping geometries in the AOI dataset,
            for an input geom_intersect. I.e. the centre of a venn diagram.

        Returns
        -------
        queryset : Django ORM Queryset
            Filtered queryset, based on URL parameters.
        """

        queryset = super().get_queryset()

        # Filter queryset based on geometry URL flags
        if geom_intersect := self.request.query_params.get("geom_intersect"):
            # TODO AreaFilter not working as expected
            # (from rest_framework_gis.filters import GeometryFilter)
            # Manual filter applied instead
            queryset = (
                queryset.filter(area__intersects=GEOSGeometry(geom_intersect))
                .annotate(
                    calculated_area=ExpressionWrapper(
                        Func("area", function="ST_Area"), output_field=FloatField()
                    )
                )
                .order_by("-calculated_area")
            )
            if self.request.query_params.get("overlap_only"):
                queryset = queryset.annotate(
                    overlap_geoms=Intersection(F("area"), GEOSGeometry(geom_intersect))
                )
        return queryset


class AOIViewSet(viewsets.ModelViewSet):
    """Display data for AOI model, in JSON format."""

    queryset = AOI.objects.all()
    serializer_class = AOISerializer

    @action(detail=False, methods=["get"])
    def calculate_polygon_area(self, request):
        """Function to calculate AOI area & annotate JSON return with value."""

        if (aoi := request.GET.get("area", None)) is not None:
            aoi = AOI.objects.filter(id=request.GET.get("id"))
            aoi_annotated = aoi.annotate(calculated_area=aoi.area.area)
            serializer = self.get_serializer_class()
            serialized = serializer(aoi_annotated)
            return Response(serialized.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class AOICreateView(generics.CreateAPIView):
    """Create a new instance of AOI model."""

    queryset = AOI.objects.all()
    serializer_class = AOISerializer


class AOIDeleteView(generics.DestroyAPIView):
    """Create a new instance of Project model."""

    queryset = AOI.objects.all()
    serializer_class = AOISerializer
