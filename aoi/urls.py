from django.urls import path

from . import views

urlpatterns = [
    path("api/aoi/", views.AOIListCreate.as_view(), name="aoi"),
    path(
        "api/aoi/list",
        views.AOIViewSet.as_view({"get": "list"}),
        name="aoi-list",
    ),
    path(
        "api/aoi/<int:pk>",
        views.AOIViewSet.as_view({"get": "retrieve"}),
        name="aoi-detail",
    ),
    path("api/aoi/create", views.AOICreateView.as_view(), name="aoi-create"),
    path(
        "api/aoi/<int:pk>/delete",
        views.AOICreateView.as_view(),
        name="aoi-create",
    ),
]
